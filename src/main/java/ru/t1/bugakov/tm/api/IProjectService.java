package ru.t1.bugakov.tm.api;

import ru.t1.bugakov.tm.model.Project;

import java.util.List;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);
}
