package ru.t1.bugakov.tm.api;

import ru.t1.bugakov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {
    void add(Project project);

    List<Project> findAll();

    void clear();
}
