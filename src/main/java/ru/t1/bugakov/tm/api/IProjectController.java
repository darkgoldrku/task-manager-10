package ru.t1.bugakov.tm.api;

public interface IProjectController {
    void createProject();

    void showProjects();

    void clearProjects();
}
