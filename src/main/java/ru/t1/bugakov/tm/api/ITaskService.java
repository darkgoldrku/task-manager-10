package ru.t1.bugakov.tm.api;

import ru.t1.bugakov.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);
}
