package ru.t1.bugakov.tm.api;

public interface ITaskController {
    void createTask();

    void showTasks();

    void clearTasks();
}
